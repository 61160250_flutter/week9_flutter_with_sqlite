import 'package:flutter/material.dart';
import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'database_provider.dart';
import 'dog.dart';
import 'dog_dao.dart';

void main() async {
  var fido = Dog(id: 0, name: 'Fido', age: 35);
  var dido = Dog(id: 1, name: 'dido', age: 22);
  await DogDao.insertDog(fido);
  await DogDao.insertDog(dido);

  print(await DogDao.dogs());

  fido = Dog(
    id: fido.id,
    name: fido.name,
    age: fido.age + 7,
  );
  await DogDao.updateDog(fido);
// Print the updated results.
  print(await DogDao.dogs()); // Prints Fido with age 42.

  await DogDao.deleteDog(0);
  print(await DogDao.dogs());
}
